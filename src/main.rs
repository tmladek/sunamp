use std::{thread, time};
extern crate clap;
use chrono::prelude::*;
use clap::{App, Arg};
use std::process::Command;

fn main() {
    let matches = App::new("SunAmp")
        .version("0.1")
        .arg(
            Arg::with_name("period")
                .short("n")
                .takes_value(true)
                .default_value("30"),
        )
        .arg(
            Arg::with_name("latitude")
                .long("latitude")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("longitude")
                .long("longitude")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("min")
                .long("min")
                .takes_value(true)
                .default_value("0"),
        )
        .arg(
            Arg::with_name("max")
                .long("max")
                .takes_value(true)
                .default_value("100"),
        )
        .arg(
            Arg::with_name("device")
                .long("device")
                .takes_value(true)
                .default_value("Master"),
        )
        .get_matches();

    let lat = matches
        .value_of("latitude")
        .unwrap()
        .parse::<f64>()
        .expect("\"latitude\" has to be a positive number!");

    let lon = matches
        .value_of("longitude")
        .unwrap()
        .parse::<f64>()
        .expect("\"latitude\" has to be a positive number!");

    let period = time::Duration::from_secs(
        matches
            .value_of("period")
            .unwrap()
            .parse::<u64>()
            .expect("\"period\" has to be a positive number!"),
    );

    let min = matches
        .value_of("min")
        .unwrap()
        .parse::<f64>()
        .expect("\"minimal offset\" has to be a positive number!");

    let max = matches
        .value_of("max")
        .unwrap()
        .parse::<f64>()
        .expect("\"minimal offset\" has to be a positive number!");

    loop {
        let now = suncalc::Timestamp(Local::now().timestamp_millis());
        let times = suncalc::get_times(now, lat, lon, None);
        let max_altitude = suncalc::get_position(times.solar_noon, lat, lon).altitude;
        let current_altitude = suncalc::get_position(now, lat, lon).altitude;

        let sun_power = (current_altitude / max_altitude * 100.0).clamp(0.00, 100.0);
        let sound_power = (sun_power / 100.0 * (max - min) + min)
            .clamp(0.0, 100.0)
            .round();

        println!(
            "CURRENT SUN POWER: {:.2}%, TRANSLATED TO SOUND POWER: {}%",
            sun_power, sound_power
        );

        let output = Command::new("amixer")
            .arg("-M")
            .arg("set")
            .arg(matches.value_of("device").unwrap())
            .arg(format!("{}%", sound_power))
            .output();

        // println!("{:?}", output.unwrap());

        match output {
            Ok(output) => {
                if !output.status.success() {
                    print!("ERROR! {}", std::str::from_utf8(&output.stderr).unwrap());
                }
            }
            Err(error) => {
                print!("ERROR! {}", error);
            }
        }

        thread::sleep(period);
    }
}
